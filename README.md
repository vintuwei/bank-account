# Bank Application Case Study

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

Install Java 8

Install Gradle 5

PS: I would have added Docker if i had enough time.

### Running The Application

Start the application by running the following gradle command

```
gradle application:bootRun
```

See a samples http requests for the 3 endpoints in the postman collection link below

```
https://www.getpostman.com/collections/5dbfa389526e5a24f7ed
```

Balance Request URL: localhost:8080/api/v1/balance/9876543210

Withdrawal:

- Method: POST

- URL: localhost:8080/api/v1/withdraw/9876543210

- Json Payload:


```
{
	"amount": {
		"value": 20000,
		"precision": 38,
		"currency": "USD"
	},
	"description": "earning withdrawal"
}
```

Deposit

- Method: POST

- URL: localhost:8080/api/v1/deposit/9876543210

- Json Payload:

```
{
	"amount": {
		"value": 20000,
		"precision": 38,
		"currency": "USD"
	},
	"description": "earning deposit"
}
```

## Tests

Run:

```
gradle test
```

## Linting

Run:

```
gradle checkstyleMain
```

### Coverage

Coverage is implemented using jacoco.

```
gradle test jacocoTestReport
```

### Coverage Verification

```
gradle test jacocoTestReport jacocoTestCoverageVerification
```

## Author

* **Vincent Tuwei** - https://github.com/vtuwei, https://github.com/vintuwei
