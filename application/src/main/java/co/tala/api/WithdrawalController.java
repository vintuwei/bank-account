package co.tala.api;

import co.tala.dto.AccountWithdrawalRequest;
import co.tala.dto.AccountWithdrawalResponse;
import co.tala.dto.Amount;
import co.tala.dto.WithdrawalRequestDTO;
import co.tala.dto.WithdrawalResponseDTO;
import co.tala.exception.AccountNotFoundException;
import co.tala.exception.WithdrawalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import co.tala.service.BankAccountService;

@RestController
public class WithdrawalController {

    @Autowired
    BankAccountService service;

    @PostMapping("/api/v1/withdraw/{accountNo}")
    public ResponseEntity<AccountWithdrawalResponse> withdrawMoney(
            @PathVariable("accountNo") String accountNo,
            @RequestBody AccountWithdrawalRequest withdrawalRequest) {

        AccountWithdrawalResponse response = new AccountWithdrawalResponse();
        response.setAccountNo(accountNo);

        WithdrawalRequestDTO withdrawalRequestDTO = new WithdrawalRequestDTO();
        withdrawalRequestDTO.getAmount().setCurrency(withdrawalRequest.getAmount().getCurrency());
        withdrawalRequestDTO.getAmount().setPrecision(withdrawalRequest.getAmount().getPrecision());
        withdrawalRequestDTO.getAmount().setValue(withdrawalRequest.getAmount().getValue());

        try {
            WithdrawalResponseDTO withdrawalResponseDTO = service.withdraw(accountNo, withdrawalRequestDTO);
            response.setAvailableBalance(new Amount()
                    .value(withdrawalResponseDTO.getAvailableBalance().getValue())
                    .precision(withdrawalResponseDTO.getAvailableBalance().getPrecision())
                    .currency(withdrawalResponseDTO.getAvailableBalance().getCurrency()));
            response.setBookBalance(new Amount()
                    .value(withdrawalResponseDTO.getBookBalance().getValue())
                    .precision(withdrawalResponseDTO.getBookBalance().getPrecision())
                    .currency(withdrawalResponseDTO.getBookBalance().getCurrency()));
            response.setStatus("SUCCESS");

        } catch (AccountNotFoundException ex) {
            response.setStatus("ERROR");
            response.setError("AccountNotFound: " + ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        } catch (WithdrawalException ex) {
            response.setStatus("ERROR");
            response.setError("WithdrawalFailed: " + ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.ok(response);
    }
}
