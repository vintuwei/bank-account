package co.tala.api;

import co.tala.dto.AccountBalanceResponse;
import co.tala.dto.AccountDTO;
import co.tala.dto.Amount;
import co.tala.exception.AccountNotFoundException;
import co.tala.service.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BalanceController {

    @Autowired
    BankAccountService service;

    @GetMapping(value = "/api/v1/balance/{accountNo}")
    public ResponseEntity<AccountBalanceResponse> getAccountBalance(@PathVariable("accountNo") String accountNo) {
        AccountBalanceResponse response = new AccountBalanceResponse();
        response.setAccountNo(accountNo);

        try {
            AccountDTO accountDTO = service.fetchAccount(accountNo);
            response.setAvailableBalance(new Amount()
                    .value(accountDTO.getAvailableBalance().intValue())
                    .currency(accountDTO.getCurrency())
                    .precision(accountDTO.getAmountPrecision().intValue()));
            response.setBookBalance(new Amount()
                    .value(accountDTO.getBookBalance().intValue())
                    .currency(accountDTO.getCurrency())
                    .precision(accountDTO.getAmountPrecision().intValue()));
            response.setStatus("SUCCESS");

        } catch (AccountNotFoundException ex) {
            response.setStatus("ERROR");
            response.setError(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.ok(response);
    }
}
