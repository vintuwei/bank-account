package co.tala.api;

import co.tala.dto.AccountDepositRequest;
import co.tala.dto.AccountDepositResponse;
import co.tala.dto.Amount;
import co.tala.dto.DepositRequestDTO;
import co.tala.dto.DepositResponseDTO;
import co.tala.exception.AccountNotFoundException;
import co.tala.exception.DepositException;
import co.tala.service.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DepositController {

    @Autowired
    BankAccountService service;

    @PostMapping("/api/v1/deposit/{accountNo}")
    public ResponseEntity<AccountDepositResponse> depositMoney(
            @PathVariable("accountNo") String accountNo,
            @RequestBody AccountDepositRequest depositRequest) {

        AccountDepositResponse response = new AccountDepositResponse();
        response.setAccountNo(accountNo);

        DepositRequestDTO depositRequestDTO = new DepositRequestDTO();
        depositRequestDTO.getAmount().setCurrency(depositRequest.getAmount().getCurrency());
        depositRequestDTO.getAmount().setPrecision(depositRequest.getAmount().getPrecision());
        depositRequestDTO.getAmount().setValue(depositRequest.getAmount().getValue());

        depositRequestDTO.setDescription(depositRequest.getDescription());

        try {
            DepositResponseDTO depositResponseDTO = service.deposit(accountNo, depositRequestDTO);
            response.setAvailableBalance(new Amount()
                    .value(depositResponseDTO.getAvailableBalance().getValue())
                    .precision(depositResponseDTO.getAvailableBalance().getPrecision())
                    .currency(depositResponseDTO.getAvailableBalance().getCurrency()));
            response.setBookBalance(new Amount()
                    .value(depositResponseDTO.getBookBalance().getValue())
                    .precision(depositResponseDTO.getBookBalance().getPrecision())
                    .currency(depositResponseDTO.getBookBalance().getCurrency()));
            response.setStatus("SUCCESS");

        } catch (AccountNotFoundException ex) {
            response.setStatus("ERROR");
            response.setError("AccountNotFound: " + ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        } catch (DepositException ex) {
            response.setStatus("ERROR");
            response.setError("DepositFailed: " + ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.ok(response);
    }
}
