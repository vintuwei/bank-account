package co.tala;

import co.tala.domain.entity.Account;
import co.tala.domain.repository.AccountRepository;
import co.tala.types.AccountType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class TestDataLoader implements ApplicationRunner {

    @Autowired
    private AccountRepository accountRepository;

    public void run(ApplicationArguments args) {

        Account revenueAccount = new Account();
        revenueAccount.setAccountNo("1234567890");
        revenueAccount.setAccountName("REVENUE_ACCOUNT");
        revenueAccount.setAccountType(AccountType.REVENUE);
        revenueAccount.setBookBalance(1000000000.00);
        revenueAccount.setAvailableBalance(1000000000.00);
        revenueAccount.setAmountPrecision(38.0);
        revenueAccount.setCurrency("USD");
        revenueAccount.setCreatedAt(new Date());

        accountRepository.save(revenueAccount);

        Account testCurrentAccount = new Account();
        testCurrentAccount.setAccountNo("9876543210");
        testCurrentAccount.setAccountName("TEST_CURRENT_ACCOUNT");
        testCurrentAccount.setAccountType(AccountType.CURRENT);
        testCurrentAccount.setBookBalance(100000.00);
        testCurrentAccount.setAvailableBalance(100000.00);
        testCurrentAccount.setAmountPrecision(38.0);
        testCurrentAccount.setCurrency("USD");
        testCurrentAccount.setCreatedAt(new Date());

        accountRepository.save(testCurrentAccount);
    }
}
