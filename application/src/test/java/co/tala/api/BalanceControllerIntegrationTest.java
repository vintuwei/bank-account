package co.tala.api;

import co.tala.BankApplication;
import co.tala.dto.AccountBalanceResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {BankApplication.class})
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BalanceControllerIntegrationTest {

    @Autowired
    protected TestRestTemplate restTemplate;
    String url = "/api/v1/balance/%s";

    @Test
    public void testFetchBalanceSuccess() {

        url = String.format(url, "9876543210");

        ResponseEntity<AccountBalanceResponse> responseEntity = restTemplate.exchange(
                url,
                HttpMethod.GET,
                new HttpEntity<>(new HttpHeaders()),
                AccountBalanceResponse.class);

        //checks on message
        AccountBalanceResponse accountBalanceResponse = responseEntity.getBody();

        assertEquals(200, responseEntity.getStatusCodeValue());
        assertEquals(accountBalanceResponse.getAvailableBalance().getValue(), 100000, 0);
    }

    @Test
    public void testFetchBalanceWithUnknownAccountNo() {

        url = String.format(url, "9876543515");

        ResponseEntity<AccountBalanceResponse> responseEntity = restTemplate.exchange(
                url,
                HttpMethod.GET,
                new HttpEntity<>(new HttpHeaders()),
                AccountBalanceResponse.class);

        //checks on message
        AccountBalanceResponse accountBalanceResponse = responseEntity.getBody();

        assertEquals(400, responseEntity.getStatusCodeValue());
        assertEquals(accountBalanceResponse.getStatus(), "ERROR");
    }
}
