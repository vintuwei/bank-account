package co.tala.api;

import co.tala.BankApplication;
import co.tala.domain.entity.Account;
import co.tala.domain.repository.AccountRepository;
import co.tala.domain.repository.AccountTransactionRepository;
import co.tala.dto.AccountDepositRequest;
import co.tala.dto.AccountDepositResponse;
import co.tala.dto.Amount;
import co.tala.types.AccountType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {BankApplication.class})
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DepositControllerIntegrationTest {

    @Autowired
    protected TestRestTemplate restTemplate;
    String url = "/api/v1/deposit/%s";
    String testAccountNo = "9876543210";

    @Autowired
    AccountTransactionRepository accountTransactionRepository;

    @Autowired
    AccountRepository accountRepository;

    @Before
    public void before() {
        accountTransactionRepository.deleteAll();
        accountRepository.deleteAll();
        Account revenueAccount = new Account();
        revenueAccount.setAccountNo("1234567890");
        revenueAccount.setAccountName("REVENUE_ACCOUNT");
        revenueAccount.setAccountType(AccountType.REVENUE);
        revenueAccount.setBookBalance(1000000000.00);
        revenueAccount.setAvailableBalance(1000000000.00);
        revenueAccount.setAmountPrecision(38.0);
        revenueAccount.setCurrency("USD");
        revenueAccount.setCreatedAt(new Date());

        accountRepository.save(revenueAccount);

        Account testCurrentAccount = new Account();
        testCurrentAccount.setAccountNo(testAccountNo);
        testCurrentAccount.setAccountName("TEST_CURRENT_ACCOUNT");
        testCurrentAccount.setAccountType(AccountType.CURRENT);
        testCurrentAccount.setBookBalance(100000.00);
        testCurrentAccount.setAvailableBalance(100000.00);
        testCurrentAccount.setAmountPrecision(38.0);
        testCurrentAccount.setCurrency("USD");
        testCurrentAccount.setCreatedAt(new Date());

        accountRepository.save(testCurrentAccount);
    }

    @Test
    public void testUnsupportedCurrency() throws URISyntaxException {

        Amount amount = new Amount()
                .currency("KES")
                .value(1000)
                .precision(38);

        AccountDepositRequest request = new AccountDepositRequest()
                .amount(amount)
                .description("test deposit");

        url = String.format(url, testAccountNo);

        RequestEntity<AccountDepositRequest> requestEntity = new RequestEntity<>(
                request, new HttpHeaders(), HttpMethod.POST, new URI(url));
        ResponseEntity<AccountDepositResponse> responseEntity = restTemplate.exchange(requestEntity, AccountDepositResponse.class);

        AccountDepositResponse accountDepositResponse = responseEntity.getBody();

        assertEquals(400, responseEntity.getStatusCodeValue());
        assertEquals(accountDepositResponse.getStatus(), "ERROR");
        assertEquals(accountDepositResponse.getError(), "DepositFailed: USD is the only supported currency");
    }

    @Test
    public void testDepositAmountErrorExceeded() throws URISyntaxException {
        Amount amount = new Amount()
                .currency("USD")
                .value(100000)
                .precision(38);

        AccountDepositRequest request = new AccountDepositRequest()
                .amount(amount)
                .description("test deposit");

        url = String.format(url, testAccountNo);

        RequestEntity<AccountDepositRequest> requestEntity = new RequestEntity<>(
                request, new HttpHeaders(), HttpMethod.POST, new URI(url));
        ResponseEntity<AccountDepositResponse> responseEntity = restTemplate.exchange(requestEntity, AccountDepositResponse.class);

        AccountDepositResponse accountDepositResponse = responseEntity.getBody();

        assertEquals(400, responseEntity.getStatusCodeValue());
        assertEquals(accountDepositResponse.getStatus(), "ERROR");
        assertEquals(accountDepositResponse.getError(), "DepositFailed: Exceeded Maximum Deposit Amount");
    }

    @Test
    public void testFrequencyLimitExceeded() throws URISyntaxException {
        Amount amount = new Amount()
                .currency("USD")
                .value(1000)
                .precision(38);

        AccountDepositRequest request = new AccountDepositRequest()
                .amount(amount)
                .description("test deposit");

        url = String.format(url, testAccountNo);

        for (int i = 0; i < 4; i++) {
            RequestEntity<AccountDepositRequest> requestEntity = new RequestEntity<>(
                    request, new HttpHeaders(), HttpMethod.POST, new URI(url));
            restTemplate.exchange(requestEntity, AccountDepositResponse.class);
        }

        RequestEntity<AccountDepositRequest> requestEntity = new RequestEntity<>(
                request, new HttpHeaders(), HttpMethod.POST, new URI(url));
        ResponseEntity<AccountDepositResponse> responseEntity = restTemplate.exchange(requestEntity, AccountDepositResponse.class);

        AccountDepositResponse accountDepositResponse = responseEntity.getBody();

        assertEquals(400, responseEntity.getStatusCodeValue());
        assertEquals(accountDepositResponse.getStatus(), "ERROR");
        assertEquals(accountDepositResponse.getError(), "DepositFailed: Transaction Frequency Limit Exceeded");
    }

    @Test
    public void testDepositPerDayExceeded() throws URISyntaxException {
        Amount amount = new Amount()
                .currency("USD")
                .value(40000)
                .precision(38);

        AccountDepositRequest request = new AccountDepositRequest()
                .amount(amount)
                .description("test deposit");

        url = String.format(url, testAccountNo);

        for (int i = 0; i < 3; i++) {
            RequestEntity<AccountDepositRequest> requestEntity = new RequestEntity<>(
                    request, new HttpHeaders(), HttpMethod.POST, new URI(url));
            restTemplate.exchange(requestEntity, AccountDepositResponse.class);
        }

        RequestEntity<AccountDepositRequest> requestEntity = new RequestEntity<>(
                request, new HttpHeaders(), HttpMethod.POST, new URI(url));
        ResponseEntity<AccountDepositResponse> responseEntity = restTemplate.exchange(requestEntity, AccountDepositResponse.class);

        AccountDepositResponse accountDepositResponse = responseEntity.getBody();

        assertEquals(400, responseEntity.getStatusCodeValue());
        assertEquals(accountDepositResponse.getStatus(), "ERROR");
        assertEquals(accountDepositResponse.getError(), "DepositFailed: Exceeded Maximum Deposit Amount Per Day");
    }

    @Test
    public void testSuccessDeposit() throws URISyntaxException {
        Amount amount = new Amount()
                .currency("USD")
                .value(1000)
                .precision(38);

        AccountDepositRequest request = new AccountDepositRequest()
                .amount(amount)
                .description("test deposit");

        url = String.format(url, testAccountNo);

        RequestEntity<AccountDepositRequest> requestEntity = new RequestEntity<>(
                request, new HttpHeaders(), HttpMethod.POST, new URI(url));
        ResponseEntity<AccountDepositResponse> responseEntity = restTemplate.exchange(requestEntity, AccountDepositResponse.class);

        AccountDepositResponse accountDepositResponse = responseEntity.getBody();

        assertEquals(200, responseEntity.getStatusCodeValue());
    }

    @Test
    public void testAccountNotFound() throws URISyntaxException {
        Amount amount = new Amount()
                .currency("USD")
                .value(100000)
                .precision(38);

        AccountDepositRequest request = new AccountDepositRequest()
                .amount(amount)
                .description("test deposit");

        url = String.format(url, testAccountNo + "1");

        RequestEntity<AccountDepositRequest> requestEntity = new RequestEntity<>(
                request, new HttpHeaders(), HttpMethod.POST, new URI(url));
        ResponseEntity<AccountDepositResponse> responseEntity = restTemplate.exchange(requestEntity, AccountDepositResponse.class);

        AccountDepositResponse accountDepositResponse = responseEntity.getBody();

        assertEquals(400, responseEntity.getStatusCodeValue());
        assertEquals(accountDepositResponse.getStatus(), "ERROR");
    }
}
