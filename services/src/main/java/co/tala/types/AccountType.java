package co.tala.types;

public enum AccountType {
    CURRENT, SAVINGS, REVENUE, RECON
}
