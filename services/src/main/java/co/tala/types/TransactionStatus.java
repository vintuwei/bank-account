package co.tala.types;

public enum TransactionStatus {
    PENDING, CLEARED, COMPLETED
}
