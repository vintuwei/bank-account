package co.tala.types;

public enum TransactionType {
    DEPOSIT, WITHDRAWAL, TRANSACTION_FEE, PENALTY, OTHER
}
