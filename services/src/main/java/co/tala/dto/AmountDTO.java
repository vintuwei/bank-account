package co.tala.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Amount
 */
public class AmountDTO {

  @NotNull
  private String currency;

  @NotNull
  private Integer precision;

  @NotNull
  private Integer value;

  public AmountDTO() {}

  public AmountDTO(Integer value, Integer precision, String currency) {
    this.value = value;
    this.precision = precision;
    this.currency = currency;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public Integer getPrecision() {
    return precision;
  }

  public void setPrecision(Integer precision) {
    this.precision = precision;
  }

  public Integer getValue() {
    return value;
  }

  public void setValue(Integer value) {
    this.value = value;
  }
}
