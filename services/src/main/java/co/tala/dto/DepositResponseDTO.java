package co.tala.dto;

public class DepositResponseDTO {

    private String accountNo;
    private AmountDTO bookBalance = null;
    private AmountDTO availableBalance = null;

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public AmountDTO getBookBalance() {
        return bookBalance;
    }

    public void setBookBalance(AmountDTO bookBalance) {
        this.bookBalance = bookBalance;
    }

    public AmountDTO getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(AmountDTO availableBalance) {
        this.availableBalance = availableBalance;
    }
}
