package co.tala.dto;

import javax.validation.Valid;

public class DepositRequestDTO {

    @Valid
    AmountDTO amount = new AmountDTO();

    String description;

    public AmountDTO getAmount() {
        return amount;
    }

    public void setAmount(AmountDTO amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
