package co.tala.dto;

import co.tala.types.AccountType;
import java.util.Date;

public class AccountDTO {

    String accountNo;

    String accountName;

    AccountType accountType;

    Double bookBalance;

    Double availableBalance;

    Double amountPrecision;

    String currency;

    Date createdAt;

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public Double getBookBalance() {
        return bookBalance;
    }

    public void setBookBalance(Double bookBalance) {
        this.bookBalance = bookBalance;
    }

    public Double getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(Double availableBalance) {
        this.availableBalance = availableBalance;
    }

    public Double getAmountPrecision() {
        return amountPrecision;
    }

    public void setAmountPrecision(Double amountPrecision) {
        this.amountPrecision = amountPrecision;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
