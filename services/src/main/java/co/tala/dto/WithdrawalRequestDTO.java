package co.tala.dto;

import javax.validation.Valid;

public class WithdrawalRequestDTO {

    @Valid
    AmountDTO amount = new AmountDTO();

    public AmountDTO getAmount() {
        return amount;
    }

    public void setAmount(AmountDTO amount) {
        this.amount = amount;
    }
}
