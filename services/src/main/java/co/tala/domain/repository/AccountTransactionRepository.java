package co.tala.domain.repository;

import co.tala.domain.entity.AccountTransaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.Date;
import java.util.List;

@Repository
public interface AccountTransactionRepository extends CrudRepository<AccountTransaction, String> {

    @Query("SELECT at FROM AccountTransaction at WHERE at.transactionType = 'DEPOSIT' and at.account.accountNo = :accountNo and at.transactionDate >= :transactionDate")
    List<AccountTransaction> findAllDepositsWithTransactionDateTimeAfter(
            @Param("accountNo") String accountNo,
            @Param("transactionDate") Date transactionDate);

    @Query("SELECT at FROM AccountTransaction at WHERE at.transactionType = 'WITHDRAWAL' and at.account.accountNo = :accountNo and at.transactionDate >= :transactionDate")
    List<AccountTransaction> findAllWithdrawalsWithTransactionDateTimeAfter(
            @Param("accountNo") String accountNo,
            @Param("transactionDate") Date transactionDate);
}
