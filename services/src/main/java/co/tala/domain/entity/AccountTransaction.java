package co.tala.domain.entity;

import co.tala.types.TransactionStatus;
import co.tala.types.TransactionType;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Entity
@Table(name = "account_transactions")
public class AccountTransaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @ManyToOne
    @JoinColumn(name = "accountId", nullable = false)
    Account account;

    @Column(name = "transactionType", nullable = false)
    @Enumerated(EnumType.STRING)
    TransactionType transactionType;

    @Column(name = "transactionStatus", nullable = false)
    @Enumerated(EnumType.STRING)
    TransactionStatus transactionStatus;

    @Column
    Double transactionAmount;

    @Column
    Double transactionAmountPrecision;

    @Column
    String transactionCurrency;

    @Column
    Double bookBalance;

    @Column
    Double availableBalance;

    @NotNull
    @CreationTimestamp
    @Column(updatable = false)
    Date transactionDate;
}
