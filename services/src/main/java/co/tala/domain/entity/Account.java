package co.tala.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import co.tala.types.AccountType;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.validator.constraints.Length;
import java.util.Date;

@Data
@Entity
@Table(name = "accounts", uniqueConstraints = { @UniqueConstraint(columnNames = {"accountNo"}) })
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @SuppressWarnings("checkstyle:MagicNumber")
    @Column(nullable = false, length = 15)
    @Length(max = 12, min = 10, message = "account length must be between 10 and 12 characters long")
    String accountNo;

    @SuppressWarnings("checkstyle:MagicNumber")
    @Column(length = 50)
    String accountName;

    @Column(name = "accountType", nullable = false)
    @Enumerated(EnumType.STRING)
    AccountType accountType;

    @Column
    @NotNull
    Double bookBalance;

    @Column
    @NotNull
    Double availableBalance;

    @NotNull(message = "amount precision cannot be null")
    @Column
    Double amountPrecision;

    @Column
    @NotNull(message = "currency cannot be null")
    String currency;

    @NotNull
    @CreationTimestamp
    @Column(updatable = false)
    Date createdAt;

    @UpdateTimestamp
    @Column
    Date updatedAt;
}
