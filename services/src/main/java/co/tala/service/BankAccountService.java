package co.tala.service;

import co.tala.domain.entity.Account;
import co.tala.domain.entity.AccountTransaction;
import co.tala.domain.repository.AccountRepository;
import co.tala.domain.repository.AccountTransactionRepository;
import co.tala.dto.AmountDTO;
import co.tala.dto.DepositRequestDTO;
import co.tala.dto.DepositResponseDTO;
import co.tala.dto.WithdrawalResponseDTO;
import co.tala.exception.AccountNotFoundException;
import co.tala.dto.AccountDTO;
import co.tala.dto.WithdrawalRequestDTO;
import co.tala.exception.DepositException;
import co.tala.exception.WithdrawalException;
import co.tala.types.TransactionStatus;
import co.tala.types.TransactionType;
import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class BankAccountService {

    // move this value to a configuration store
    Integer maxWithdrawAmount = 20000;

    Integer maxDepositAmount = 40000;

    Integer maxDepositFrequecy = 4;

    Integer maxWithdrawalFrequency = 3;

    Integer maxDepositAmountPerDay = 150000;

    Integer maxWithdrawalAmountPerDay = 50000;

    String revenueAccountNo = "1234567890";

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    AccountTransactionRepository accountTransactionRepository;

    public AccountDTO fetchAccount(String accountNo) throws AccountNotFoundException {

        Optional<Account> optionalAccount = accountRepository.findByAccountNo(accountNo);

        if (!optionalAccount.isPresent()) {
            throw new AccountNotFoundException(String.format("Account %s not found", accountNo));
        }

        AccountDTO accountDTO = new AccountDTO();
        BeanUtils.copyProperties(optionalAccount.get(), accountDTO);

        return accountDTO;
    }

    public WithdrawalResponseDTO withdraw(String accountNo, @Valid WithdrawalRequestDTO dto)
            throws AccountNotFoundException, WithdrawalException {

        if (!dto.getAmount().getCurrency().equals("USD")) {
            throw new WithdrawalException("USD is the only supported currency");
        }

        // TODO - transaction limit values should be moved to config store
        if (dto.getAmount().getValue() > maxWithdrawAmount) {
            throw new WithdrawalException("Exceeded Maximum Withdrawal Per Transaction");
        }

        Optional<Account> accountOptional = accountRepository.findByAccountNoForUpdate(accountNo);

        if (!accountOptional.isPresent()) {
            throw new AccountNotFoundException(String.format("Account %s not found", accountNo));
        }

        Account account = accountOptional.get();

        if (account.getAvailableBalance() < dto.getAmount().getValue()) {
            throw new WithdrawalException("Insufficient Balance");
        }

        DateTime today = new DateTime().withTimeAtStartOfDay();

        List<AccountTransaction> todaysTransactions = accountTransactionRepository
                .findAllWithdrawalsWithTransactionDateTimeAfter(accountNo, today.toDate());

        if (todaysTransactions.size() == maxWithdrawalFrequency) {
            throw new WithdrawalException(String.format("Transaction Frequency Limit Exceeded"));
        }

        Double transactionAmountToday = 0.0;

        for (int i = 0; i < todaysTransactions.size(); i++) {
            transactionAmountToday += todaysTransactions.get(i).getTransactionAmount();
        }

        if (transactionAmountToday + Double.valueOf(dto.getAmount().getValue()) > maxWithdrawalAmountPerDay) {
            throw new WithdrawalException(String.format("Exceeded Maximum Withdrawal Amount Per Day"));
        }

        // debit user's account
        // credit revenue account

        account.setAvailableBalance(account.getAvailableBalance() - Double.valueOf(dto.getAmount().getValue()));
        account.setBookBalance(account.getBookBalance() - Double.valueOf(dto.getAmount().getValue()));
        account.setUpdatedAt(new Date());

        AccountTransaction transaction = createAccountTransaction(account, TransactionType.WITHDRAWAL, dto.getAmount());
        accountTransactionRepository.save(transaction);
        accountRepository.save(account);

        // debit revenue account
        // TODO - enable multiple updates to revenue account
        Account revenueAccount = accountRepository.findByAccountNoForUpdate(revenueAccountNo).get();
        revenueAccount.setAvailableBalance(revenueAccount.getAvailableBalance() + Double.valueOf(dto.getAmount().getValue()));
        revenueAccount.setBookBalance(revenueAccount.getBookBalance() + Double.valueOf(dto.getAmount().getValue()));
        revenueAccount.setUpdatedAt(new Date());

        AccountTransaction revenueTransaction = createAccountTransaction(revenueAccount, TransactionType.DEPOSIT, dto.getAmount());
        accountTransactionRepository.save(revenueTransaction);
        accountRepository.save(revenueAccount);

        WithdrawalResponseDTO responseDTO = new WithdrawalResponseDTO();
        responseDTO.setAccountNo(accountNo);
        responseDTO.setAvailableBalance(new AmountDTO(account.getAvailableBalance().intValue(),
                account.getAmountPrecision().intValue(), account.getCurrency()));
        responseDTO.setBookBalance(new AmountDTO(account.getBookBalance().intValue(),
                account.getAmountPrecision().intValue(), account.getCurrency()));

        return responseDTO;
    }

    // TODO - move validation logic out of this function to reduce the number of lines
    public DepositResponseDTO deposit(String accountNo, @Valid DepositRequestDTO dto)
            throws AccountNotFoundException, DepositException {

        if (!dto.getAmount().getCurrency().equals("USD")) {
            throw new DepositException("USD is the only supported currency");
        }

        if (dto.getAmount().getValue() > maxDepositAmount) {
            throw new DepositException("Exceeded Maximum Deposit Amount");
        }

        Optional<Account> accountOptional = accountRepository.findByAccountNoForUpdate(accountNo);

        if (!accountOptional.isPresent()) {
            throw new AccountNotFoundException(String.format("Account %s not found", accountNo));
        }

        Account account = accountOptional.get();

        DateTime today = new DateTime().withTimeAtStartOfDay();

        List<AccountTransaction> todaysTransactions = accountTransactionRepository
                .findAllDepositsWithTransactionDateTimeAfter(accountNo, today.toDate());

        if (todaysTransactions.size() == maxDepositFrequecy) {
            throw new DepositException(String.format("Transaction Frequency Limit Exceeded"));
        }

        Double transactionAmountToday = 0.0;

        for (int i = 0; i < todaysTransactions.size(); i++) {
            transactionAmountToday += todaysTransactions.get(i).getTransactionAmount();
        }

        if (transactionAmountToday + Double.valueOf(dto.getAmount().getValue()) > maxDepositAmountPerDay) {
            throw new DepositException(String.format("Exceeded Maximum Deposit Amount Per Day"));
        }

        // credit user's account
        // debit revenue account

        account.setAvailableBalance(account.getAvailableBalance() + Double.valueOf(dto.getAmount().getValue()));
        account.setBookBalance(account.getBookBalance() + Double.valueOf(dto.getAmount().getValue()));
        account.setUpdatedAt(new Date());

        AccountTransaction transaction = createAccountTransaction(account, TransactionType.DEPOSIT, dto.getAmount());
        accountTransactionRepository.save(transaction);
        accountRepository.save(account);

        // debit revenue account
        // TODO - enable multiple updates to revenue account
        Account revenueAccount = accountRepository.findByAccountNoForUpdate(revenueAccountNo).get();
        revenueAccount.setAvailableBalance(revenueAccount.getAvailableBalance() - Double.valueOf(dto.getAmount().getValue()));
        revenueAccount.setBookBalance(revenueAccount.getBookBalance() - Double.valueOf(dto.getAmount().getValue()));
        revenueAccount.setUpdatedAt(new Date());

        AccountTransaction revenueTransaction = createAccountTransaction(revenueAccount, TransactionType.WITHDRAWAL, dto.getAmount());
        accountTransactionRepository.save(revenueTransaction);
        accountRepository.save(revenueAccount);

        DepositResponseDTO responseDTO = new DepositResponseDTO();
        responseDTO.setAccountNo(accountNo);
        responseDTO.setAvailableBalance(new AmountDTO(account.getAvailableBalance().intValue(),
                account.getAmountPrecision().intValue(), account.getCurrency()));
        responseDTO.setBookBalance(new AmountDTO(account.getBookBalance().intValue(),
                account.getAmountPrecision().intValue(), account.getCurrency()));
        return responseDTO;
    }

    private AccountTransaction createAccountTransaction(Account account, TransactionType type, AmountDTO transactionAmount) {
        AccountTransaction transaction = new AccountTransaction();
        transaction.setAccount(account);
        transaction.setAvailableBalance(account.getAvailableBalance());
        transaction.setAvailableBalance(account.getBookBalance());
        transaction.setTransactionAmount(Double.valueOf(transactionAmount.getValue()));
        transaction.setTransactionCurrency(transactionAmount.getCurrency());
        transaction.setTransactionAmountPrecision(Double.valueOf(transactionAmount.getPrecision()));
        transaction.setTransactionDate(new Date());
        transaction.setTransactionType(type);
        transaction.setTransactionStatus(TransactionStatus.COMPLETED); // TODO - handle transaction states
        return transaction;
    }
}
