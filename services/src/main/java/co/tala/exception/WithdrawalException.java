package co.tala.exception;

public class WithdrawalException extends Exception {

    public WithdrawalException(String msg) {
        super(msg);
    }

    public WithdrawalException(String msg, Exception e) {
        super(msg + " because of " + e.toString());
    }
}
