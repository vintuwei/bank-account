package co.tala.exception;

public class DepositException extends Exception {

    public DepositException(String msg) {
        super(msg);
    }

    public DepositException(String msg, Exception e) {
        super(msg + " because of " + e.toString());
    }
}
