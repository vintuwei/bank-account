package co.tala.exception;

/**
 * Thrown when an account is not found
 */
public class AccountNotFoundException extends Exception {

    public AccountNotFoundException(String msg) {
        super(msg);
    }

    public AccountNotFoundException(String msg, Exception e) {
        super(msg + " because of " + e.toString());
    }
}
